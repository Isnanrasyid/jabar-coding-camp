//soal 1
var string1 = "saya sangat senang hari ini";
var string2 = "belajar javascript itu keren";
//jawaban soal 1
console.log(`${string1.substring(0, 4)} ${string1.substring(12, 18)} ${string2.substring(0, 7)} ${string2.substring(8, 18).toUpperCase()}`)

//soal 2
var str1 = "10";
var str2 = "2";
var str3 = "4";
var str4 = "6";
var int1 = parseInt(str1);
var int2 = parseInt(str2);
var int3 = parseInt(str3);
var int4 = parseInt(str4);
//jawaban soal 2
console.log((int2 * int3) + int1 + int4)

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4 , 14);
var kataKetiga = kalimat.substring(15 , 18);
var kataKeempat = kalimat.substring(19 , 24);
var kataKelima = kalimat.substring(25 , 31);
//jawaban soal 3
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima)
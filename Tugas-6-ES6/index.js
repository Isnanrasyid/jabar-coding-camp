//soal 1
const luaspersegipanjang = (a,b) => {
    console.log ( a*b ) ;
}
luaspersegipanjang(2,3); //6
const kelilingpersegipanjang = (a,b) => {
    console.log (2*(a+b)) ;
}
kelilingpersegipanjang(2,3); //10

//soal 2
const newFunction = (firstName, lastName) => {
    return {
      firstName,
      lastName,
        fullName: () => {
        console.log(firstName + " " + lastName)
      }
    }
  }
  newFunction("William", "Imoh").fullName() //William Imoh
//soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby) //Muhammad Iqbal Mubarok Jalan Ranamanyar playing football
//soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined) // ['Will', 'Chris','Sam',  'Holly','Gill', 'Brian','Noel', 'Maggie']
//soal 5
const planet = "earth" 
const view = "glass" 
const before = `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet} `
console.log(before) //Lorem glass dolor sit amet consectetur adipiscing elit earth
//soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
for(i = 0; i < daftarHewan.length; i++){
	daftarHewan.sort();
    console.log(daftarHewan[i]);
} 
//soal 2
var data = {name : "Isnan" , age : 18 , address : "Baleendah Kab. Bandung" , hobby : "Gaming" }
function introduce(nama, age, address, hobby) {
	return "Nama saya " + nama +", umur saya " + age + " tahun , Alamat saya di" + address + ", dan saya punya hobby yaitu " + hobby
}
var perkenalan = introduce(data.name, data.age, data.address, data.hobby)
console.log(perkenalan)

//soal 3
function hitung_huruf_vokal(nama) {
	return nama.match(/[aeiou]/gi).length;
}
var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

//soal 4
function hitung(angka) {
	return angka * 2 - 2
}

console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));
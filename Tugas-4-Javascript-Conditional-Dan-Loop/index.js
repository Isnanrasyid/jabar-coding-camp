//soal 1
var nilai = 40;
if (nilai >= 85) {
	console.log("indeksnya A")
}else if ((nilai >=75 ) && (nilai < 85)) {
	console.log("indeksnya B")
}else if ((nilai >=65 ) && (nilai < 75)) {
	console.log("indeksnya C")
}else if ((nilai >=55 ) && (nilai < 65)) {
	console.log("indeksnya D")
}else {
	console.log("indeksnya E")
} //jawaban soal 1

//soal 2
var tanggal = 1;
var bulan = 4;
var tahun = 2003;
switch (bulan) {
  case 1:   { console.log('ini 1 Januari 2003'); break; }
  case 2:   { console.log('ini 1 Februari 2003'); break; }
  case 3:   { console.log('ini 1 Maret 2003'); break; }
  case 4:   { console.log('ini 1 April 2003'); break; }
  case 5:   { console.log('ini 1 Mei 2003'); break; }
  case 6:   { console.log('ini 1 Juni 2003'); break; }
  case 7:   { console.log('ini 1 Juli 2003'); break; }
  case 8:   { console.log('ini 1 Agustus 2003'); break; }
  case 9:   { console.log('ini 1 September 2003'); break; }
  case 10:   { console.log('ini 1 Oktober 2003'); break; }
  case 11:   { console.log('ini 1 November 2003'); break; }
  case 12:   { console.log('ini 1 Desember 2003'); break; }
  default:  { console.log('Bulan yang anda masukkan salah'); }
} //jawaban soal 2

//soal 3
var s = '';
var n = 7;
for(var h = 1; h <= n; h++){
 for(var i = 1; i <= h; i++){
  s += '#';
 }
 s += '\n';   
}
console.log(s);

//soal 4
var m=10;
var languages = ["","I love programming", "I love Javascript", "I love VueJS"];
for(var angka = 1; angka <= m; angka++) {
  console.log(angka +' - ' + languages[angka]);
} //jawaban soal 4


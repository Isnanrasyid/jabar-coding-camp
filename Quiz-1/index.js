//soal 1 
function jumlah_kata(kalimat) {
    return kalimat.trim().split(/\s+/).length;
}
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = "Saya Iqbal"

console.log(jumlah_kata(kalimat_1)) //6
console.log(jumlah_kata(kalimat_2)) //2

//soal 2

function next_date(tanggal,bulan,tahun) {
    return new Date(tanggal, bulan, tahun);
}
var tanggal = 28
var bulan = 2
var tahun = 2020
console.log(next_date(tanggal, bulan, tahun));